const createError = require('http-errors');
const mongoose = require('mongoose');

// const Product = require('../Models/Product.model');
const User = require('../Models/user.model')


// API for the create USER
  const createUser = async (req, res, next) => {
    try {
      const user = new User(req.body);
      const result = await user.save();
      res.send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === 'ValidationError') {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  }

  // API for the LIST PRODUCT
  const getUser =  async (req, res, next) => {
    try {
      const id = req.params.id;
      const result = await User.find();
      // const product = await Product.findOne({ _id: id });
      if (!result) {
        throw createError(404, 'Product does not exist.');
      }
      res.send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid Product id'));
        return;
      }
      next(error);
    }
  }

  // API for LIST PRODUCT BY ID 
  const findUserById =  async (req, res, next) => {
    
    try{
      const id = req.params.id;
      const product = await User.findById(id);
      // const product = await Product.findOne({ _id: id });
      if (!product) {
        throw createError(404, 'Product does not exist.');
      }
      res.send(product);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid Product id'));
        return;
      }
      next(error);
    }
  }


  // API for the UPDATE PRODUCT
 const updateAUser =  async (req, res, next) => {
    try {
      const id = req.params.id;
      const updates = req.body;
      const options = { new: true };

      const result = await User.findByIdAndUpdate(id, updates, options);
      if (!result) {
        throw createError(404, 'Product does not exist');
      }
      res.send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        return next(createError(400, 'Invalid Product Id'));
      }

      next(error);
    }
  }

// API for the delete product
  const deleteAUser = async (req, res, next) => {
    
    try {
      const id = req.params.id;
      const result = await User.findByIdAndDelete(id);
      // console.log(result);
      if (!result) {
        throw createError(404, 'Product does not exist.');
      }
      res.send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid Product id'));
        return;
      }
      next(error);
    }
  }


  module.exports = {
    createUser,
    deleteAUser,
    updateAUser,
    findUserById,
    getUser
  };
